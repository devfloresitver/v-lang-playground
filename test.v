// import os //imports on top, outside
const (
	pi = 3.14
)

fn main() {
	a, b := foo()
	println(a) // 2
	println(b) // 3
	add(10, 20)
	mut me := 'me'
	me = 'literally me'
	println(me)
	println(me[0..9]) // indexing produces a byte
	emoji := `🚀` // character literals use backticks, their type is rune
	println(emoji) // prints �
	// emoji := r`🚀` cannot use `r` (raw string) with `byte` and `rune`
	str := r'🚀' // cannot use `r` (raw string) with `byte` and `rune`
	println(str) // prints just fine
	astr := r'🚀'
	// interpolation
	println(astr)
	println('rocket $astr')
	println('rocket $str')
	println('rocket $emoji')
	println('30 + 92 > 100 ${add(30, 92) > 100}')
	lengthy_sum := '3510.2032 + 0.5922 = ${(3510.2032 + 0.5922):.2f}'
	println('$lengthy_sum:10')
	// /string ops
	mut s := 'testan'
	s += ' vlang'
	println(s)
	this_year := 'my age is ' + 10.str()
	println(this_year)
	hex_num1 := 0x2A
	hex_num2 := 0x2A
	println('hex sum ${hex_num1 + hex_num2}')
	mut large_array := []int{}
	n_elements := 10000
	println('printing ' + n_elements.str() + ' elements')
	for i in 0 .. n_elements {
		// large_array[i] = i
		large_array << i
	}
	chunk := large_array[100..200]
	println('is 3000 in large_array ${3000 in large_array}')
	println(chunk)
	// impairs := large_array.filter( it % 2 != 0) //it is a builtin variable
	/*
	doesn't work?
	double_them := impairs.map(fn (n int) string {
		return n.str()
	})
	*/
	/*
	double_them := impairs.map(fn (n int) int {
		return n * 2
	})
	println(double_them)
	*/
	mut two_dim := [][]int{len: 3, init: []int{len: 4}}
	two_dim[0][0] = 1
	frst := two_dim[0][0]
	println(frst)
	for i in 0 .. 3 {
		for j in 0 .. 4 {
			two_dim[i][j] = i * j
			println(' i $i j  $j ${two_dim[i][j]}')
		}
	}
	println(two_dim)
	large_array.sort(a > b) // doesn't return a new array
	// println(large_array)
	mut grades_map := map[string]int{}
	grades_map['math'] = 100
	grades_map['science'] = 50
	println(grades_map)
	grades_map.delete('science')
	println(grades_map)
	grades := {
		'math': 1
		'science': 2
	}
	println(grades)
	/*
	thing := os.input("enter something ")
	println("$thing so he said")

	res := if large_array.len < 2000 {
						large_array.len
					} else{
						0
					}

	println(res)
	*/
	math := 'math' in grades
	println(math)
	for n in large_array {
		println(n)
	}
	for i, n in large_array {
		println('$i - $n')
	}
	for key, value in grades_map {
		println('$key -> $value') // _ will ignore the param
	}
	// If you need to modify the array while looping,
	// you have to use indexing
	outstanding_score := grades_map['math']
	println(outstanding_score)
	opinion := match outstanding_score {
		90...100 { true } // have to return bool
		0...50 { false }
		else { false }
	}
	println(opinion)
	// higher order functions example, neat
	lil_num := 100
	// like Scala's getOrElse, but kinda weird
	hof := plus_and_ten_times(lil_num, plus_one, ten_times) or { 0 }
	println(hof)
}

fn ten_times(n int) int {
	return n * 10
}

fn plus_one(n int) int {
	return n + 1
}

fn plus_and_ten_times(n int, f fn (int) int, g fn (int) int) ?int {
	if n > 0 {
		return f(g(n))
	}
	// return error("must be higher than 0")
}

fn add(x int, y int) int {
	return x + y
}

fn foo() (int, int) {
	return 2, 3
}
