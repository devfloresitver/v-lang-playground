fn main() {
	mut large_array := []int{}
	n_elements := 1000
	println('printing ' + n_elements.str() + ' elements')
	for i in 0 .. n_elements {
		// println(i)
		// large_array[i] = i //will trigger panic
		large_array <<
			(plus_and_ten_times(i, plus_one, ten_times) * (plus_and_ten_times(i, ten_times, plus_one)))
	}
	large_array.sort(a > b)
	println(large_array)
}
[live]
fn ten_times(n int) int {
	return n * 10
}
[live]
fn plus_one(n int) int {
	return n + 1
}
[live]
fn plus_and_ten_times(n int, f fn (int) int, g fn (int) int) int {
	if n > 0 {
		return f(g(n))
	}
	return 0
}
